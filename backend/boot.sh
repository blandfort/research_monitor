#!/bin/sh
python wait_for_elasticsearch.py
exec gunicorn -b 0.0.0.0:80 --timeout 120 server:app
