# DATABASE
#
# We use the database to store Contents.
# Each Content typically represents a research paper or similar publication.
#
import logging
import datetime
import numpy as np
import re
import os
import json
import psycopg2
import psycopg2.extras
from psycopg2.errors import UndefinedTable
from elasticsearch import Elasticsearch

from content import Content

log = logging.getLogger(__name__)


class DB:

    # Newest date needs to be stored class variable for efficiency reasons
    newest_date = None

    index_table = 'indecs'
    index_fields = ['title', 'abstract', 'authors', 'published']
    content_table = 'contents'
    content_fields = ['id_', 'title', 'abstract', 'published', 'source', 'category']
    author_table = 'authors'

    # Glossary
    abbreviation_table = 'abbreviations'
    abbreviation_fields = ['day', 'acronym', 'definition', 'paper_id']

    # Trends
    trend_table = 'trends'
    trend_fields = ['query', 'score', 'occurrences']

    def __init__(self):
        # Connect to the Postgres database
        self.connection = psycopg2.connect(user = os.environ['DATABASE_USERNAME'],
                                      password = os.environ['DATABASE_PASSWORD'],
                                      host = os.environ['DATABASE_HOST'],
                                      port = os.environ['DATABASE_PORT'],
                                      database = os.environ['DATABASE_NAME'])
        self.cursor = self.connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Queries to create the necessary tables
        self.content_table_query = f'''CREATE TABLE IF NOT EXISTS {self.content_table}
            (id_ SERIAL,
             title          VARCHAR(256)    NOT NULL,
             abstract   TEXT DEFAULT NULL,
             category   VARCHAR(64) DEFAULT NULL,
             source VARCHAR(128)    NOT NULL,
             published   DATE DEFAULT NULL, UNIQUE (source));'''
        self.author_table_query = f'''CREATE TABLE IF NOT EXISTS {self.author_table}
            (content_id BIGINT NOT NULL,
             author     VARCHAR(256) NOT NULL,
             position INT,
             UNIQUE (content_id, author));'''
        self.abbreviation_table_query = f'''CREATE TABLE IF NOT EXISTS {self.abbreviation_table}
            (day        DATE NOT NULL,
            acronym       VARCHAR(16) NOT NULL,
            definition  VARCHAR(256),
            paper_id    INTEGER NOT NULL,
            UNIQUE (acronym, paper_id));'''
        self.trend_table_query = f'''CREATE TABLE IF NOT EXISTS {self.trend_table}
            (query      VARCHAR(256)    NOT NULL,
            score       REAL,
            occurrences INTEGER,
            UNIQUE (query));'''

        # Connect to elasticsearch
        self.es = Elasticsearch([{'host': os.environ['ELASTICSEARCH_HOST'], 'port': os.environ['ELASTICSEARCH_PORT']}])

        self.index_table_body = {
            "mappings": {
                "properties": {
                    "title": { "type": "text" },
                    "abstract": { "type": "text" },
                    "authors": { "type": "text" },
                    "published": { "type": "date", "format": "yyyy-MM-dd" },
                }
            }
        }

        # Initialize newest date
        if DB.newest_date is None:
            try:
                DB.newest_date = self._get_newest_date()
            except UndefinedTable:
                log.warning("Couldn't initialize newest date since database is not populated yet.")
                self.connection.reset()
                DB.newest_date = None

    def _ensure_tables(self):
        # Make sure the table for Contents exists
        self.cursor.execute(self.content_table_query)
        # And for author information
        self.cursor.execute(self.author_table_query)
        # Make sure the table for glossary information exists
        self.cursor.execute(self.abbreviation_table_query)
        # Make sure the trends table exists
        self.cursor.execute(self.trend_table_query)
        self.connection.commit()

        # Make sure the table for indexing exists
        self.es.indices.create(index=self.index_table, body=self.index_table_body, ignore=400)
        # Ignore already-exists exception

    def __enter__(self):
        return self

    def set_up(self):
        """Run this function once when starting the backup to make sure all the tables exist
        in the database."""
        log.info("Setting up database ...")
        self._ensure_tables()

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Disconnecting from database
        if(self.connection):
            self.cursor.close()
            self.connection.close()
            log.info("Connection to the PostgreSQL database is closed.")

            self.es.close()
            log.info("Connection to Elasticsearch is closed.")

    def __del__(self):
        self.connection.close()
        self.es.close()

    def _author_contents(self, author):
        """Given an author name, return all content IDs where the author is listed."""
        query = f"SELECT * FROM {self.author_table} WHERE author LIKE %s;"
        self.cursor.execute(query, [author]) 
        return [row['content_id'] for row in self.cursor]

    def _where_query(self, ids=None, date=None, from_date=None, to_date=None, order_by=None, reverse_order=False, category="cs.", max_num=100000, offset=0, author=None, group_by=None):
        constraints = []
        constraint_values = []

        if ids is not None:
            constraints.append("id_=ANY('{%s}'::int[])" % ','.join(map(str, ids)))
            #constraint_values.append(ids)

        if date is not None:
            constraints.append("published=%s")
            constraint_values.append(date)
        elif from_date is not None or to_date is not None:
            if from_date is not None:
                constraints.append("published>=%s")
                constraint_values.append(from_date)
            if to_date is not None:
                constraints.append("published<=%s")
                constraint_values.append(to_date)

        if category is not None:
            constraints.append("category LIKE %s")
            constraint_values.append(category+'%')

        # Formulate the query
        query = ''

        if len(constraints):
            query += 'WHERE '
            query += ' AND '.join(constraints)
            query += ' '

        if group_by is not None:
            query += "GROUP BY "+group_by+" "

        if order_by is not None:
            query += 'ORDER BY '+order_by+' '

            if reverse_order:
                query += 'DESC '

        query += f"OFFSET {int(offset)} LIMIT {int(max_num)} "

        return query, constraints, constraint_values

    def get(self, query=None, date=None, keyword=None, order_by=None, reverse_order=False, author=None,
            max_num=10000, offset=0, category="cs.", from_date=None, to_date=None, **kwargs):
        if query is not None:
            return self.search(query=query, date=date, order_by=order_by, reverse_order=reverse_order, max_num=max_num, category=category, from_date=from_date, to_date=to_date, offset=offset, author=author, **kwargs)
        elif keyword is not None:
            return self.search(query=f'"{keyword}"', date=date, order_by=order_by, reverse_order=reverse_order, max_num=max_num, category=category, from_date=from_date, to_date=to_date, offset=offset, author=author, **kwargs)

        select = ','.join([self.content_table+'.'+field for field in self.content_fields])
        query = f"SELECT {select},string_agg({self.author_table}.author, ',' ORDER BY {self.author_table}.position) as authors "
        query += f"FROM {self.content_table} INNER JOIN {self.author_table} "
        query += f"ON {self.content_table}.id_={self.author_table}.content_id "

        if author is not None:
            content_ids = self._author_contents(author=author)
        else:
            content_ids = None

        q, constraints, constraint_values = self._where_query(from_date=from_date, to_date=to_date, date=date, category=category, max_num=max_num, order_by=order_by, reverse_order=reverse_order, offset=offset, group_by=select, ids=content_ids)
        query += q

        # Run the query
        self.cursor.execute(query, constraint_values)

        result = []
        for row in self.cursor:
            info = {key: row[key] for key in self.content_fields}
            info['authors'] = row['authors'].split(',')
            result.append(Content(**info))
        return result

    def count(self, **kwargs):
        return len(self.get(**kwargs))

    def _es_date_formatting(self, date):
        return "%d-%0.2d-%0.2d"%(date.year, date.month, date.day)

    def _es_query(self, query, date=None, from_date=None, to_date=None, return_as_json=False, max_num=None, author=None, **kwargs):
        """Formulate the query body for an ElasticSearch query."""
        q = {
            "query": {
                "bool": {
                    "must": [
                        {
                        #"multi_match": {
                        #"query_string": {
                        "simple_query_string": {
                            "query": query,
                            "fields": [ "title", "abstract" ],
                            "default_operator": "and",
                        }}
                    ]
                }
            }
        }

        if date is not None or from_date is not None or to_date is not None:
            if date is not None:
                date_filter = { "match": { "published": date if not return_as_json else self._es_date_formatting(date) } }
            else:
                date_filter = {"range": { "published": {} } }
                if from_date is not None:
                    date_filter["range"]["published"]["gte"] = from_date if not return_as_json else self._es_date_formatting(from_date)
                if to_date is not None:
                    date_filter["range"]["published"]["lte"] = to_date if not return_as_json else self._es_date_formatting(to_date)

            q["query"]["bool"]["must"].append(date_filter)

        if author is not None:
            author_filter = { "match": { "authors": author } }
            q["query"]["bool"]["must"].append(author_filter)

        if max_num is not None:
            q["size"] = max_num

        return q if not return_as_json else json.dumps(q)


    def search(self, query, **kwargs):
        order_by = kwargs.get('order_by', None)
        if 'reverse_order' in kwargs and kwargs['reverse_order']:
            order = 'desc'
        else:
            order = 'asc'

        max_num = int(kwargs.get('max_num', 10000))
        if max_num>10000:
            #TODO implement paging instead
            log.warning("Maximum result number of %d requested, but ElasticSearch can return at most 10k results and paging is not implemented. (Might not get all results.)"%max_num)
            max_num = 10000
        res = self.es.search(body=self._es_query(query, **kwargs),
                                sort=order_by+':'+order if order_by is not None else None,
                                size=max_num, from_=kwargs.get("offset", None), filter_path=['hits.hits._id'])

        if not res:
            return []

        ids = [doc['_id'] for doc in res['hits']['hits']]
        #TODO might be cleaner to also consider the fields from elasticsearch directly

        def get_contents_by_id(ids, **kwargs):
            select = ','.join([self.content_table+'.'+field for field in self.content_fields])
            query = f"SELECT {select},string_agg({self.author_table}.author, ',' ORDER BY {self.author_table}.position) as authors "
            query += f"FROM {self.content_table} INNER JOIN {self.author_table} "
            query += f"ON {self.content_table}.id_={self.author_table}.content_id "

            q, constraints, constraint_values = self._where_query(ids=ids, group_by=select, **kwargs)
            query += q
    
            self.cursor.execute(query, constraint_values)

            contents = []
            for row in self.cursor:
                info = {key: row[key] for key in self.content_fields}
                info['authors'] = row['authors'].split(',')
                contents.append(Content(**info))
            return contents

        contents = get_contents_by_id(ids, **kwargs)
        return contents

    def add(self, content, update_newest_date=True):
        modified_content_fields = [field for field in self.content_fields if field!='id_']

        # Add to the content table
        entry_values = [str(content[field]) for field in modified_content_fields]

        query = "INSERT INTO %s (%s) VALUES " % (self.content_table, ','.join(modified_content_fields))
        query += "(%s)" % ','.join(["%s" for _ in entry_values])
        query += " ON CONFLICT DO NOTHING RETURNING id_;"
        #TODO instead of DO NOTHING, could also update the row
        # (see https://www.postgresqltutorial.com/postgresql-upsert/)
        self.cursor.execute(query, entry_values)
        new_row = self.cursor.fetchone()

        # If we didn't add any new content, we don't want to update the index or add author info
        if new_row:
            id_of_new_row = new_row[0]

            # Add author entries
            for pos,author in enumerate(content['authors']):
                query = f"INSERT INTO {self.author_table} (content_id, author, position) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING;"
                self.cursor.execute(query, (id_of_new_row, author, pos+1))

            # Add the element to the index
            self.es.index(index=self.index_table, id=id_of_new_row, body={field: content[field] for field in self.index_fields})

            self.connection.commit()

        if update_newest_date:
            DB.newest_date = self._get_newest_date()

    def recreate_index(self, batch_size=1000):
        log.info("Starting to recreate ElasticSearch index ...")

        # First delete the search index       
        self.es.indices.delete(index=self.index_table, ignore=[400, 404]) 
        # Then create it again
        self.es.indices.create(index=self.index_table, body=self.index_table_body)

        # Now add all the papers to the index
        offset = 0
        while True:
            # Note: We have to order by ID here.
            # There seems to be a bug with PostgreSQL or psycopg2 causing us to miss
            # elements if the position is not uniquely determined for all elements.
            elements = self.get(max_num=batch_size, offset=offset, order_by='id_')

            if len(elements)<1:
                break

            # Formulate a bulk request
            request = []
            for c in elements:
                r = json.dumps({"index": {"_id": c["id_"]}})
                r += '\n'+json.dumps({field: c[field] if type(c[field]) is not datetime.date else self._es_date_formatting(c[field])
                                         for field in self.index_fields})
                request.append(r)

            # Run the bulk request
            self.es.bulk(body='\n'.join(request), index=self.index_table)

            offset += batch_size
        log.info("Finished recreating ElasticSearch index.")

    def get_abbreviations(self, date):
        query = f"SELECT {','.join(self.abbreviation_fields)} FROM {self.abbreviation_table} "
        query += "WHERE day=%s;"

        abbreviations = []
        self.cursor.execute(query, [date])
        for row in self.cursor:
            abbr = {'acronym': row[self.abbreviation_fields.index('acronym')],
                        'definition': row[self.abbreviation_fields.index('definition')],
                        'paper_id': row[self.abbreviation_fields.index('paper_id')]}
            abbreviations.append(abbr)
        return abbreviations

    def add_abbreviation(self, abbreviation):
        entry_values = [abbreviation[field] for field in self.abbreviation_fields]
        query = "INSERT INTO %s (%s) VALUES " % (self.abbreviation_table, ','.join(self.abbreviation_fields))
        query += "(%s)" % ','.join(["%s" for _ in entry_values])
        query += " ON CONFLICT DO NOTHING;"
        self.cursor.execute(query, entry_values)
        self.connection.commit()

    def get_glossary(self, offset=0, max_num=1000000, startswith=None):
        query = "SELECT DISTINCT ON (acronym) acronym, definition, COUNT(paper_id) "
        query += f"FROM {self.abbreviation_table} "
        if startswith is not None:
            query += f"WHERE acronym LIKE '{startswith}%' "
        query += "GROUP BY acronym, definition "
        query += f"ORDER BY acronym, COUNT(paper_id) "
        query += f"LIMIT {max_num} OFFSET {offset}"
        self.cursor.execute(query)
        return [{'acronym': row['acronym'], 'definition': row['definition']} for row in self.cursor]

    def get_glossary_term(self, acronym):
        query = f"SELECT definition, COUNT(paper_id) FROM {self.abbreviation_table} "
        query += "WHERE acronym=%s"
        query += "GROUP BY definition ORDER BY COUNT(paper_id) DESC;"
        self.cursor.execute(query, [acronym])
        definitions = [row['definition'] for row in self.cursor]
        return {'acronym': acronym, 'definitions': definitions}

    def get_glossary_names(self):
        query = f"SELECT DISTINCT acronym FROM {self.abbreviation_table} ORDER BY acronym;"
        self.cursor.execute(query)
        return [row['acronym'] for row in self.cursor]

    def count_glossary_terms(self):
        query = f"SELECT COUNT(DISTINCT acronym) FROM {self.abbreviation_table};"
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        return result['count']

    def add_trend(self, trend):
        entry_values = [trend[field] for field in self.trend_fields]
        query = "INSERT INTO %s (%s) VALUES " % (self.trend_table, ','.join(self.trend_fields))
        query += "(%s)" % ','.join(["%s" for _ in entry_values])
        query += " ON CONFLICT (query) DO UPDATE SET score=%s, occurrences=%s"
        self.cursor.execute(query, entry_values+[trend['score'], trend['occurrences']])
        self.connection.commit()

    def get_trends(self, query=None, order_by="query", reverse_order=False, max_num=10000, offset=0):
        q = f"SELECT * FROM {self.trend_table} "
        if query is not None:
            q += "query=%s "
        q += f"ORDER BY {order_by} "
        if reverse_order:
            q += "DESC "
        q += f"LIMIT {max_num} OFFSET {offset}"

        self.cursor.execute(q, [query] if query is not None else [])
        results = self.cursor.fetchall()
        return results

    def _get_newest_date(self):
        """Get the date of the most recent content in our database."""
        most_recent_contents = self.get(order_by='published', max_num=1, reverse_order=True)
        if len(most_recent_contents):
            return most_recent_contents[0]['published']
        else:
            return None

    def get_trend(self, query=None, from_date=None, to_date=None):
        if to_date is None:
            to_date = datetime.date.today()

        # Only consider dates till most recent paper we have
        # (otherwise we get a bias in the trend coefficient, getting more negative for popular concepts)
        if DB.newest_date is not None and to_date>DB.newest_date:
            to_date = DB.newest_date

        total_days = (to_date-from_date).days + 1
        date_counts = {date: 0 for date in (from_date + datetime.timedelta(n) for n in range(total_days))}

        occurrences = self.es.search(body=self._es_query(query, from_date=from_date, to_date=to_date),
                                sort='published:asc',
                                size=10000, filter_path=['hits.hits._id', 'hits.hits._source.published'])

        if occurrences:
            for occ in occurrences['hits']['hits']:
                date = datetime.date(*map(int, occ['_source']['published'].split('-')))
                date_counts[date] += 1

        dates = sorted(list(date_counts.keys()))

        fracs = [date_counts[date] for date in dates]
        trend = np.array(fracs)
        return dates, trend

    def get_bulk_trend(self, queries, from_date=None, to_date=None):
        if to_date is None:
            to_date = datetime.date.today()

        # Only consider dates till most recent paper we have
        # (otherwise we get a bias in the trend coefficient, getting more negative for popular concepts)
        if DB.newest_date is not None and to_date>DB.newest_date:
            to_date = DB.newest_date

        total_days = (to_date-from_date).days + 1

        # Formulate a bulk request
        request = []
        for q in queries:
            # Request header
            r = '{}\n'
            # Request body
            r += self._es_query(query=q, from_date=from_date, to_date=to_date, max_num=10000, return_as_json=True).replace('\n', ' ')

            request.append(r)

        # Run the bulk request
        occurrences = self.es.msearch(body='\n'.join(request), index=self.index_table)

        # Compile the trend information
        trends = {}
        dates = [from_date + datetime.timedelta(n) for n in range(total_days)]
        for ix,occs in enumerate(occurrences['responses']):
            date_counts = {date: 0 for date in dates}

            if occs:
                for occ in occs['hits']['hits']:
                    date = datetime.date(*map(int, occ['_source']['published'].split('-')))
                    date_counts[date] += 1

            fracs = [date_counts[date] for date in dates]
            trend = np.array(fracs)
            trends[queries[ix]] = (dates, trend)

        return trends
