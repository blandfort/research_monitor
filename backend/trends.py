import logging
import os
import datetime
import numpy as np
from scipy.stats import linregress

from database import DB


log = logging.getLogger(__name__)

today = datetime.date.today()



class Trends:
    """Class to handle trend information."""

    def __init__(self):
        pass

    def update(self, concepts=None, extra_concepts=[], batch_size=1000):
        """Take a list of queries and add the resulting information to the precomputed
        trend dictionaries."""
        if concepts is None:
            concepts = self.get_concepts()
        concepts = list(set(concepts+extra_concepts))

        log.info("Updating trend information for %d concepts ..."%len(concepts))
        with DB() as db:

            # Emerging trends
            log.info("Computing emerging trends ...")
            week_setting = os.environ['NUM_WEEKS_FOR_TRENDS']
            
            # Figure out the date of the most recent paper in the database
            newest_date = DB.newest_date

            if newest_date is not None:
                from_date = newest_date - datetime.timedelta(weeks=int(week_setting))
            else:
                from_date = today - datetime.timedelta(weeks=int(week_setting))

            # Compute the trendiness
            for ix in range(0, len(concepts), batch_size):
                self._compute_trendiness(db, queries=concepts[ix:ix+batch_size], from_date=from_date, store_result=True)

    def _compute_trendiness(self, db, queries, from_date=None, to_date=None, store_result=False):
        trends = db.get_bulk_trend(queries=queries, from_date=from_date, to_date=to_date)

        scores = {}
        for query in trends:
            _,trend = trends[query]

            total_count = int(np.sum(trend))

            if len(trend)<2:
                log.warning("No sufficient data available to compute trend for query '%s'!"%query)
                return None, total_count

            # Fit the trend linear to get slope and use slope as trendiness value
            score = linregress(range(len(trend)), trend).slope

            result = {'query': query, 'score': score, 'occurrences': total_count}
            if store_result:
                log.debug("Storing trend information for query '%s' ..."%query)
                db.add_trend(result)
            scores[query] = result

        return scores

    def get_concepts(self, reverse_order=False, max_num=100000, offset=0):
        """Get a list of all concepts for which there is trend information."""
        with DB() as db:
            results = db.get_trends(order_by='query', reverse_order=reverse_order, max_num=max_num, offset=offset)
        return [r['query'] for r in results]

    def get_trendiness(self, reverse_order=False, max_num=10, offset=0):
        with DB() as db:
            results = db.get_trends(order_by='score', reverse_order=not reverse_order,
                                    max_num=max_num, offset=offset)
        return [{'name': r['query'], 'score': r['score']} for r in results]

    def get_occurrences(self, reverse_order=False, max_num=10, offset=0):
        with DB() as db:
            results = db.get_trends(order_by='occurrences', reverse_order=not reverse_order,
                                    max_num=max_num, offset=offset)
        return [{'name': r['query'], 'occurrence_count': r['occurrences']} for r in results]

