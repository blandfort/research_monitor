import os
import logging
from elasticsearch import Elasticsearch
from time import sleep


log = logging.getLogger(__name__)


def elasticsearch_is_ready():
    es = Elasticsearch([{'host': os.environ['ELASTICSEARCH_HOST'], 'port': os.environ['ELASTICSEARCH_PORT']}])

    if not es.ping():
        return False
    else:
        return True


import logger

while True:
    if elasticsearch_is_ready():
        break
    else:
        log.info("ElasticSearch not ready yet. Waiting for some time before starting backend ...")
        sleep(5)
