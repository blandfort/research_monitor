# Research Monitor

_Research Monitor (RM) is a service to facilitate staying up to date in computer science research and finding research papers that are relevant to your interests._


## Documentation

In `docs/`, you can find some further documentation:

- [PLANNING.md](docs/PLANNING.md) describes the direction we shall move into
- See [CONTRIBUTING.md](docs/CONTRIBUTING.md) for information on how to contribute


## Deploying the Service

First of all:

- Clone the repository
- Create a local environment file (`cp .env.example .env`) and adjust the settings in `.env`
- In `frontend/app/templates` copy `legal.example.html` to `legal.html` and adjust the information there
- Build the docker images:
    - Go to backend directory, run `docker build --tag rmbackend:<version> .`
    - Go to frontend directory, run `docker build --tag rmfrontend:<version> .`

Then running the code:

- Run the docker daemon (from the base directory of the repo): `dockerd`
- Run the service using docker compose: `docker-compose -f docker-compose.yml up backend frontend`
- The service can then be accessed using an HTTP endpoint (see `backend/server.py`)
    - Use `/update` (POST) for updating the data (e.g. `curl localhost:8000/update --data "type=xml"`)
    - Use `/get` (GET) for retrieving data (e.g. `curl 'localhost:8000/get?date=2020.8.12&max_num=5'`)
- To browse the contents using the frontend, open http://localhost:8000 in your web browser
- To complete the set up, create a cron job to run the update every day, by calling `crontab -e` and adding the line
```
    0 6 * * 1-5 curl http://localhost:8000/update --data ""
```

Note that the docker container will save its data to the host system permanently.


### Updating the Database and Extracted Info

In most cases, it should be sufficient to call `/update` without passing any arguments.
This is also done by the script [update.sh](update.sh).

- By default `/update` runs an "update all", updating all components:
    - 'xml': Papers are scraped and stored in XML format
    - 'db': The new papers are put into the database
    - 'glossary': Based on the database, the glossary is updated
    - 'trends': For all glossary terms (spelled out versions), trends are computed
- An argument `type` can be passed to `/update` to only update one of the four components
- A date range can be passed to consider older dates as well (e.g. `/update` with `from=2020.1.1` to update all dates since beginning of 2020)
    - Initially, a range should be used to populate the server with data
    - On each day, calling without date range is sufficient to update everything
    - If daily update was missed, use date range to fill the gap in the database
- Additionally, `/reindex` can be called to recreate the ElasticSearch index (i.e. the whole index is deleted and created again based on the elements in the database)

See [update.py](backend/update.py) for details.


## Environment Variables

When running with `docker-compose`, environment variables are automatically set (see also [.env](.env)).
Otherwise, be mindful of the environment variables expected by the docker containers.

The docker container of the backend expects certain environment variables to be set:

- Postgres settings: `DATABASE_USERNAME`, `DATABASE_PASSWORD`, `DATABASE_PORT`, `DATABASE_NAME`, `DATABASE_HOST`
- Elasticsearch settings: `ELASTICSEARCH_HOST`, `ELASTICSEARCH_PORT`
- `NUM_WEEKS_FOR_TRENDS`

Frontend expects following environment variables:

- `BACKEND_URL`
- `NUM_WEEKS_TO_PLOT`


## Information Flow

Data is stored at different stages:

1. XML: when papers are crawled they are stored in the original XML format returned by the arXiv API (the main reason for this is to avoid re-crawling any data if changes in the database are made)
2. Database: papers are then imported into a database for faster accessibility
3. Glossary: glossary terms are computed from the database and stored as pickle file
4. Trend information is currently computed for the spelled out glossary terms, and stored as JSON files

Note that the UI makes use of auxiliary files and the database.

The XML files are only used to populate the database.


## Links

- [NLP Scholar by Saif M. Mohammad](http://saifmohammad.com/WebPages/nlpscholar.html)
    - Project to analyze NLP publications
    - Organizes papers by author and conference
    - Uses numbers of citations
    - Several publications which include some of the technical details
- [arXiv Dataset on Kaggle](https://www.kaggle.com/Cornell-University/arxiv)
    - Contains 1.7 Million articles
    - Data extracted by using mattbierbaum's repository
    - Linked to three tasks "title prediction by the abstract", "creating a survey from related article and papers" and "multi-label text classification"
- [mattbierbaum's "arXiv public datasets" repository](https://github.com/mattbierbaum/arxiv-public-datasets) 
    - This project also downloads paper PDFs to AWS (needs a lot of space!)
    - Paper PDFs are used to extract full text and citation graph
    - Includes a file `authors.py` to get author affiliations
- [Karpathy's arXiv Sanity Preserver](http://arxiv-sanity.com/)
    - Shows most recent papers
    - Also considers how many tweets mentioned a paper to get hyped papers
    - Has feature to show similar papers to a given one (seems to be based on TF-IDF vectors)
    - Has recommendations as well (can create account and login)
    - Open-source on github: https://github.com/karpathy/arxiv-sanity-preserver
- [arXiv blog](https://blog.arxiv.org/)
- [CORE](https://core.ac.uk/)
    - "World's largest collection of open access research papers"
    - Offers an API as well (free for research, but rate-limited)
