# Notes on Contributing


## Guidelines

- Focus on a few valuable features rather than having many bells and whistles


## Git Workflow

Our branching model is can be seen as a simplified version of the one described 
on [nvie's blog](https://nvie.com/posts/a-successful-git-branching-model/).

**In short**:
We use one permanent branch `develop`, which is the main branch.
For introducing any changes we add new temporary branches,
push these branches to the repo and merge them on gitlab by using merge
requests.

**Some more details**:
For contributing you typically do the following:

- Make sure you are on the `develop` branch: `git checkout develop`
- Update your local `develop` branch: `git pull origin develop`
- Create a new local branch for introducing changes and switch to that branch:
  `git checkout -b BRANCH_NAME` (as `BRANCH_NAME` choosing any short title
  that describes what you will change)
- Repeat the following steps until what you intended to change is complete
  (and everything is working fine in your local branch):
    1. Change the code or other files in your local repo
    2. Use `git add` to stage changes for commit,
      i.e. to tell git to consider these changes for the next commit
      (`git add .` to consider all the changes you made)
    3. Commit the changes: `git commit`
- Push your local branch to the repo on gitlab: `git push origin BRANCH_NAME`
  (note that this step can also be done earlier for backing up some intermediate
  state, or when working together with someone else on a single feature)
- On gitlab create a merge request for merging the changes of your new branch
  into `develop`.
- Handling the merge request is a good time for reviewing and discussing
  the changes, and making sure nothing breaks when merging it
- After the merge was successful update your local repo:
    - Switch to `develop`: `git checkout develop`
    - Update your `develop` branch (so that it now includes the changes
      introduced in the new branch): `git pull origin develop`
    - Remove the branch from your local repo: `git branch -d BRANCH_NAME`
- Repeat all these steps until the code is complete ;)


