import requests
import os
import json
import datetime
from urllib.parse import urljoin

URL = os.environ['BACKEND_URL']

#TODO might wanna do some error handling

def get_data(dataname, **kwargs):
    response = requests.get(urljoin(URL, dataname), params=kwargs)
    return json.loads(response.text)

def get_papers(**kwargs):
    response = requests.get(urljoin(URL, 'get'), params=kwargs)

    papers = response.text.split('\n')
    return [json.loads(paper) for paper in papers if len(paper.strip())]

def get_glossary(**kwargs):
    return get_data(dataname='glossary', **kwargs)

def get_trending(**kwargs):
    return get_data(dataname='trending', **kwargs)

def get_popular(**kwargs):
    """Get the most common concepts."""
    return get_data(dataname='popular', **kwargs)

def get_concept(name):
    return get_data(dataname='concept', name=name)

def get_concept_names():
    names = requests.get(urljoin(URL, 'concept_names')).text
    return [name for name in names.split('\n') if name.strip()]

def get_last_updated():
    response = requests.get(urljoin(URL, 'updated'))
    date = datetime.date(*map(int, response.text.split('-')))
    return date

def get_trend(**kwargs):
    response = requests.get(urljoin(URL, 'trend'), params=kwargs)
    return json.loads(response.text)
