import os
import xml.etree.ElementTree as ET
import logging
import datetime

from content import Content

PAPER_DIRECTORY = '/usr/share/researchmonitor/xml/'

log = logging.getLogger(__name__)


def arxiv_date_to_date(date):
    parts = date.split('T')[0].split('-')
    return tuple(map(int, parts))

def read_paper_xml(filepath):
    root = ET.parse(open(filepath, encoding='latin1')).getroot()
    #root = ET.parse(open(filepath, 'r')).getroot()

    papers = {}
    
    for type_tag in root.findall('{http://www.w3.org/2005/Atom}entry'):
        id_stuff = type_tag.findall('{http://www.w3.org/2005/Atom}id')
        if len(id_stuff)<1:
            continue
        
        paper_id = id_stuff[0].text
        paper = {key: type_tag.findall('{http://www.w3.org/2005/Atom}%s'%key)[0].text
                 for key in ['title', 'published', 'summary']}
        paper['category'] = type_tag.findall('{http://www.w3.org/2005/Atom}category')[0].attrib['term']
        paper['id'] = paper_id

        # Add author info
        paper['authors'] = []
        for stuff in type_tag.findall('{http://www.w3.org/2005/Atom}author'):
            paper['authors'].append(stuff.findall('{http://www.w3.org/2005/Atom}name')[0].text)

        element = Content(title=paper['title'], abstract=paper['summary'], published=datetime.date(*arxiv_date_to_date(paper['published'])), category=paper['category'], authors=paper['authors'], source=paper['id'])

        papers[paper_id] = element
        
    return papers


def read_month_papers(year, month, out_dir=PAPER_DIRECTORY):
    """Read all papers from a certain month and return as dict.
    
    Paper IDs are used as keys."""
    papers = {} # paper ID as key
    
    month_dir = f'{year}_{month:02d}'
    if os.path.isdir(os.path.join(out_dir, month_dir)):
        for name in os.listdir(os.path.join(out_dir, month_dir)):
            papers.update(read_paper_xml(os.path.join(out_dir,month_dir,name)))

    log.info("%d papers found for month %d in year %d"%(len(papers), month, year))
    return papers

