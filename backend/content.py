import datetime
import json


class Content:
    """Class to create and manage Content elements to represent scientific resources.
    """

    @classmethod
    def day_formatting(cls, d):
        return f"{d.year}-{d.month:02d}-{d.day:02d}"

    @classmethod
    def string_to_day(cls, s):
        return datetime.date(*map(int, s.split('-')))

    def __init__(self, source, title, abstract=None, published=None, id_=None, category=None, authors=None, **kwargs):
        self._data = {}

        if id_ is not None:
            self['id_'] = id_

        self['title'] = title.strip()

        if abstract is not None:
            self['abstract'] = abstract.strip()

        if published is not None:
            #assert type(published) in [datetime.date, datetime.datetime]
            assert type(published) is datetime.date, "Attribute 'published' must be of type datetime.date, but is of type "+str(type(published))+"!"
            self['published'] = published

        self['source'] = source

        if category is not None:
            self['category'] = category.strip()

        if authors is not None:
            self['authors'] = tuple(authors)

        for key,val in kwargs.items():
            self[key] = val

    def __setitem__(self, key, value):
        self._data[key] = value

    def __getitem__(self, key):
        return self._data[key]

    def __repr__(self):
        return repr(self._data)

    def to_dict(self):
        return self._data

    @classmethod
    def from_dict(cls, d):
        return Content(**d)

    def to_json(self):
        info = self.to_dict()

        # Replace date objects with strings
        for field in info.keys():
            if type(info[field]) is datetime.date:
                info[field] = self.day_formatting(info[field])

        return json.dumps(info)

    @classmethod
    def from_json(cls, js):
        info = json.loads(js)

        # Parse some special values
        info['published'] = self.string_to_day(info['published'])
        return cls(**info)
