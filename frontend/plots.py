import configparser
import datetime
import json
import os

import logger

from data import get_last_updated, get_trend

config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read('config.ini')


def query_plot(query, div_id='plotDiv', from_date=None):
    if from_date is None:
        # Instead of today get most recent day when system was updated
        last_updated = get_last_updated()
        from_date = last_updated - datetime.timedelta(weeks=int(os.environ['NUM_WEEKS_TO_PLOT']))

    paper_dates, fracs = get_trend(query=query, from_date=from_date, to_date=last_updated)
    #fracs, paper_dates = make_a_trace(concept_name)

    trace = "var trace = {"
    trace += "x:[{}],".format(', '.join(["'%s'"%pd for pd in paper_dates]))
    trace += "y:[{}],".format(', '.join(map(str,fracs)))
    trace += """
    type: 'scatter'
    };

    var data = [trace];"""

    return make_plot_code(data=trace, div_id=div_id)


toy_data = """
var trace = {
    x:['2020-10-04', '2021-11-04', '2023-12-04'],
    y: [90, 40, 60],
    type: 'scatter'
};

var data = [trace];
"""

def make_plot_code(data=toy_data, plotsize="width:800px;height:400px;",
        div_id='plotDiv'):
    code = """<script>
%s

var layout = {
  title: {
    text:'Mentions over Time',
    font: {
      family: 'Courier New, monospace',
      size: 20
    },
    xref: 'paper',
    x: 0.05,
  },
  yaxis: {
    title: {
      text: '#Papers with Concept',
      font: {
        family: 'Courier New, monospace',
        size: 18,
        color: '#7f7f7f'
      }
    }
  }
};

Plotly.newPlot('%s', data, layout, {scrollZoom: true});
</script>
""" % (data, div_id)

    return code
