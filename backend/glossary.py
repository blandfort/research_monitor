import logging
import os
import datetime
import json
import re
import pickle
from collections import Counter

from database import DB

log = logging.getLogger(__name__)

today = datetime.date.today()



abbreviation_seed_pattern = r'\([A-Z\-]{2,}\)'


class Glossary:
    """Glossary with definitions of terms."""

    def __init__(self):
        pass

    def get(self, offset=0, max_num=1000000, startswith=None):
        with DB() as db:
            glossary = db.get_glossary(offset=offset, max_num=max_num, startswith=startswith)
        return glossary

    def get_names(self):
        with DB() as db:
            acronyms = db.get_glossary_names()
        return acronyms

    def get_concept(self, concept_name):
        with DB() as db:
            concept = db.get_glossary_term(concept_name)
        return concept

    def __len__(self):
        with DB() as db:
            count = db.count_glossary_terms()
        return count

    def update(self, earliest_date, latest_date):
        """Update the glossary information based on all papers of the given
        date range.
        """
        if type(latest_date) is datetime.datetime:
            latest_date = latest_date.date()
        if type(earliest_date) is datetime.datetime:
            earliest_date = earliest_date.date()

        log.info("Precomputing matches ...")
        total_days = (latest_date-earliest_date).days + 1
        with DB() as db:
            for n in range(total_days):
                date = earliest_date + datetime.timedelta(days=n)
                self._compute_day_abbreviation_matches(db, date=date)

        log.info("%d terms in glossary"%len(self))

    def _concept_occurrence_query(self, concept):
        """Given a concept formulate a search query to be used for finding papers with the concept.

        The resulting papers are used to calculate trendiness, first mentions and occurrence counts."""
        definitions = [concept['definition']] #concept['definitions'][0]
        # NOTE: Could consider extending to other definitions.
        
        query = ' OR '.join(['"'+defn+'"' for defn in definitions])
        return query

    def _compute_day_abbreviation_matches(self, db, date, **kwargs):
        log.info("Precomputing abbreviations for day %s ..."%date)
        paper_list = db.get(date=date, **kwargs)

        abbreviations = []
        for paper_id,paper_content in [(paper['id_'], paper['title']+'.\n'+paper['abstract']) for paper in paper_list]:
            for sentence in re.split(r'[.!?]\s*', paper_content):
                matches = re.findall(abbreviation_seed_pattern, sentence)

                for match in matches:
                    abbr = match.strip('()')

                    # Find the explanation
                    expl = re.search(r'(?P<definition>((?P<capword>[A-Z][\w]+)[\s-]+){%d})\(%s\)'%(len(abbr),abbr),
                                     re.sub(r'[\s]+', ' ', sentence))

                    if expl:
                        definition = expl.group('definition').rstrip()

                        abbreviation = {'acronym': abbr, 'definition': definition, 'paper_id': paper_id}
                        abbreviations.append(abbreviation)

                        # Add the abbreviation to the database
                        extended_abbreviation = {'day': date}
                        extended_abbreviation.update(abbreviation)
                        db.add_abbreviation(extended_abbreviation)
        
        return abbreviations


