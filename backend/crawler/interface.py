from crawler.scrape import scrape_arxiv_month
from crawler.xml_loader import read_month_papers

import logging
import datetime

log = logging.getLogger(__name__)

today = datetime.date.today()


class XMLInterface:
    """Interface class to download papers using the arXiv API in XML format
    and to read the local XML files."""

    def __init__(self):
        pass

    def update(self, earliest_month, latest_month=None, **kwargs):
        """Scrape all arXiv papers for a range of months.

        Papers are stored locally in XML format.

        Arguments:
        - earliest_month: Earliest month to scrape, given in datetime.date format
        - latest_month: Latest month to scrape; if None, current month is used
        """
        if latest_month is None:
            latest_month = today

        log.info("Starting to scrape papers between months %d.%d and %d.%d ..." % (earliest_month.month, earliest_month.year, latest_month.month, latest_month.year))
        for year in range(earliest_month.year, latest_month.year+1):
            if year==earliest_month.year:
                start_month = earliest_month.month
            else:
                start_month = 1

            if year==latest_month.year:
                end_month = latest_month.month
            else:
                end_month = 12

            for month in range(start_month, end_month+1):
                log.info("Scraping papers for month %d, year %d ..." % (month, year))
                scrape_arxiv_month(month=month, year=year, **kwargs)

    def read(self, earliest_date, latest_date=None, category_startswith='cs.', categories=None, **kwargs):
        """Get all locally stored arXiv papers for a given range of months.

        Arguments:
        - earliest_date: Earliest date to read, given in datetime.date format
        - latest_date: Latest date to read; if None, current date is used
        - categories: If list of categories, only consider papers from these categories
        - category_startswith: Only consider papers of categories starting with this string
                        (Ignored if categories is given)

        Returns: list of Content elements representing the papers
        """
        if latest_date is None:
            latest_date = today

        # Convert to datetime if necessary to avoid exception when comparing to paper['published'] (which is of type date)
        if type(earliest_date) is datetime.datetime:
            earliest_date = earliest_date.date()
        if type(latest_date) is datetime.datetime:
            latest_date = latest_date.date()

        papers = {}

        for year in range(earliest_date.year, latest_date.year+1):
            if year==earliest_date.year:
                start_month = earliest_date.month
            else:
                start_month = 1

            if year==latest_date.year:
                end_month = latest_date.month
            else:
                end_month = 12

            for month in range(start_month, end_month+1):
                month_papers = read_month_papers(month=month, year=year, **kwargs)

                # Filtering the papers by category
                if categories is not None:
                    month_papers = {paper_id: paper for paper_id,paper in month_papers.items()
                                  if paper['category'] in categories}
                elif category_startswith is not None:
                    month_papers = {paper_id: paper for paper_id,paper in month_papers.items()
                                  if paper['category'].startswith(category_startswith)}

                # Filtering by day
                month_papers = {paper_id: paper for paper_id,paper in month_papers.items()
                                if earliest_date<=paper['published']<=latest_date}

                papers.update(month_papers)

        return list(papers.values())
