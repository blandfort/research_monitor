import datetime
import time
import urllib.request
import json
import os
import logging
import xml.etree.ElementTree as ET

from crawler.xml_loader import read_month_papers, PAPER_DIRECTORY


log = logging.getLogger(__name__)


def latest_scraped_number(year, month, category=None, out_dir=PAPER_DIRECTORY):
    latest_num = 0
    
    # subdirectories depend on year
    if year<2007 or (year==2007 and month<=3):
        out_path = os.path.join(out_dir, f"{year}_{month:02d}", category)
    else:
        out_path = os.path.join(out_dir, f"{year}_{month:02d}")
        
    for root, dirs, files in os.walk(out_path, topdown=False):
        for name in files:

            earliest, latest = name.rsplit('.',1)[0].split('-')
            latest = int(latest)
            
            if latest>=latest_num:
                latest_num = latest
    
    return latest_num


def scrape_arxiv_month(year=datetime.datetime.now().year, month=datetime.datetime.now().month,
                       stepsize=100, out_dir=PAPER_DIRECTORY, category=None):
    """Use the arXiv API to collect information about published papers and writes it to a file.

    FIXME: There are issues with older format.
    
    Arguments:
    - categories: For older papers, IDs vary across categories.
                In this case, all categories from the given list are considered."""
    
    for _ in range(100000):
        t = time.time()

        start = latest_scraped_number(year=year, month=month, category=category)+1
        numbers = range(start, start+stepsize)

        new_papes = scrape_arxiv_papers(year, month, numbers=numbers, category=category, out_dir=out_dir)
        
        t2 = time.time()
        if t2-t<3:  # make sure we don't send a request more than once every 3s
            time.sleep(3-(t2-t))
        
        if new_papes<1:  # Crawling (probably) finished for this category
            log.info(f"Finished scraping for year {year}, month {month} " + (', %s'%category if category else ''))
            break
    #log.info("Converting data to JSON ...")
    #convert_data(year=year, month=month)

    
def scrape_arxiv_papers(year, month, numbers, category=None, out_dir=PAPER_DIRECTORY):
    if year<2007 or (year==2007 and month<=3):
        # old format (July 1991 - March 2007): category/YYMMNNN
        id_list = [f'{category}/{year%100:02d}{month:02d}{ident:03d}' for ident in numbers]
        out_path = os.path.join(out_dir, f"{year}_{month:02d}", category)
    else:
        # "identifiers up to month 1412 are zero-padded to 4-digits in the last block,
        # and those from 1501 onward are zero-padded to 5-digits"
        if year<2015:
            id_list = [f'{year%100:02d}{month:02d}.{ident:04d}' for ident in numbers]
        else:
            id_list = [f'{year%100:02d}{month:02d}.{ident:05d}' for ident in numbers]
        out_path = os.path.join(out_dir, f"{year}_{month:02d}")

    url = 'http://export.arxiv.org/api/query?id_list='+','.join(id_list)+'&amp;start=0&amp;max_results=1000'

    data = urllib.request.urlopen(url).read()
    
    #root = ET.parse(io.BytesIO(data)).getroot()
    root = ET.fromstring(data.decode('latin1'))

    return_num = 0
    for type_tag in root.findall('{http://www.w3.org/2005/Atom}entry'):
        return_num += 1 if len(type_tag.findall('{http://www.w3.org/2005/Atom}id'))>0 else 0
    
    if return_num>0:
        start = numbers[0]
        filename = "%d-%d.xml"%(start, start+return_num-1)
        
        # create directories if not existing
        if not os.path.exists(out_path):
            os.makedirs(out_path)
        
        with open(os.path.join(out_path,filename), 'wb') as f:
            f.write(data)
            
    return return_num
