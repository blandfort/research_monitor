from flask import render_template
from flask import request
from app import app
import math
import logging
import json
import re
import urllib

from plots import query_plot
from data import get_glossary, get_papers, get_concept, get_concept_names, get_last_updated, get_trending, get_popular


log = logging.getLogger(__name__)


# For concept page we currently limit the number of including papers to show
max_papers = 20


@app.route('/')
@app.route('/index')
def index():
    log.info("/index requested.")

    most_trending = get_trending(max_num=10)
    for concept in most_trending:
        concept['url'] = urllib.parse.quote(concept['name'], safe='')
    least_trending = get_trending(reverse_order=True, max_num=10)
    for concept in least_trending:
        concept['url'] = urllib.parse.quote(concept['name'], safe='')
    popular_concepts = get_popular(max_num=30)
    for concept in popular_concepts:
        concept['url'] = urllib.parse.quote(concept['name'], safe='')

    last_updated = get_last_updated()

    return render_template('index.html', last_updated=last_updated,
                most_trending=most_trending, 
                least_trending=least_trending,
                popular_concepts=popular_concepts)

@app.route('/legal')
def legal():
    log.info("/legal requested.")

    return render_template('legal.html', title='Legal Information | Research Monitor')

@app.route('/privacy')
def privacy():
    log.info("/privacy requested.")

    return render_template('privacy.html', title='Data Privacy Policy | Research Monitor')

@app.route('/about')
def about():
    log.info("/about requested.")

    return render_template('about.html', title='About Research Monitor')

@app.route('/glossary')
def glossary():
    log.info("/glossary requested.")

    pages = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')

    if 'startswith' in request.args and request.args['startswith'] in pages:
        sw = request.args['startswith']
    else:
        sw = pages[0]

    glossary_list = get_glossary(startswith=sw)

    page = pages.index(sw)
    
    # Pagination
    lower_pages = pages[:page]
    higher_pages = pages[page+1:]

    return render_template('glossary.html', glossary=glossary_list, page=sw, lower_pages=lower_pages, higher_pages=higher_pages,
            title='Glossary | Research Monitor')

@app.route('/search')
def search():
    log.info("/search requested.")

    req = request.args['query']

    # Creating a pseudo concept
    concept = {}
    concept['acronym'] = re.sub(r'[^\w]', ' ', req).strip()  # bit of security
    concept_query = '"'+concept['acronym']+'"'

    if len(concept['acronym'])<1:
        return '' #TODO do some nicer error handling

    # Mentioning papers
    papers = get_papers(query=concept_query, order_by='published',
                    reverse_order=True, max_num=max_papers)

    # Trend curve
    plot_divid = 'trendPlot'
    return render_template('concept.html', concept=concept, papers=papers, plot=query_plot(query=concept_query, div_id=plot_divid), plot_divid=plot_divid)

@app.route('/concept')
def concept():
    log.info("/concept requested.")

    req = request.args['name']

    # bit of security
    if req in get_concept_names():
        concept = get_concept(req)

        concept_query = '"'+concept['definitions'][0]+'"'
        # NOTE: This needs to be in line with the concept query in backend/glossary/interface.py
        #TODO this dependency between the two parts is not optimal of course

        # Mentioning papers
        papers = get_papers(query=concept_query, order_by='published',
                        reverse_order=True, max_num=max_papers)

        # Trend curve
        plot_divid = 'trendPlot'
        return render_template('concept.html', concept=concept, papers=papers, plot=query_plot(query=concept_query, div_id=plot_divid), plot_divid=plot_divid)
    else:
        return '' #TODO do some nicer error handling

