import requests
import os
import json
import datetime
from urllib.parse import urljoin

URL = 'http://localhost:8000' #os.environ['BACKEND_URL']


def get_data(dataname, **kwargs):
    response = requests.get(urljoin(URL, dataname), params=kwargs)
    return json.loads(response.text)


def get_papers(**kwargs):
    response = requests.get(urljoin(URL, 'get'), params=kwargs)

    papers = response.text.split('\n')
    return [json.loads(paper) for paper in papers if len(paper.strip())]

def get_glossary(**kwargs):
    return get_data(dataname='glossary', **kwargs)

def get_trend(**kwargs):
    response = requests.get(urljoin(URL, 'trend'), params=kwargs)
    return json.loads(response.text)

def get_last_updated():
    response = requests.get(urljoin(URL, 'updated'))
    date = datetime.date(*map(int, response.text.split('-')))
    return date
