# Planning


## Next Steps

- Finish the first release
- Discuss:
    - Who should be the main target audience? Researchers, developers from industry or consultants?
    - Which directions are most promising to learn in the process and get a valuable product?
    - Figure out how other people can contribute
- Launch RM as a non-profit service as soon as possible
    - Ask for donations on the website or some crowd websites
    - Ask people to join


## Direction

- Do as non-profit
- Mainly help people to
    - Stay up to date with research (e.g. by visualizing trends)
    - Find relevant papers


### Specific Options

- !Find influential papers (based on citations)
    - Goal: help find all the must-reads on a certain topic
- Papers by institution
    - Goal: make it easy to follow the big players (assuming that they do most the important innovations)
- Give overview for topics or subcategories
    - Extract various information like tasks, methods and datasets, cluster concepts etc. so that a user can quickly get an overview of a topic
    - Note that paperswithcode.com does something quite similar
    - Ultimately, automatically create something like a survey on the topic
- Bridging gaps between fields:
    - Find related papers from other fields or categories to inspire folks
    - Extend to different fields and journals
- Describe trends better
    - Compute trends based on clusters of concepts for a better overview
    - Detect early on when there is something new that is likely to become popular (i.e. try to predict as well, based on past trend data of concept and papers they were occurring in)
- Entertainment
    - Consider tweets and similar sources to make it more like 9gag for research
- Personalization
    - Compile an overview of contents most relevant to a specific user
- Help writing papers
    - Suggest related literature
    - Find related ideas and applications from other fields
    - Find out what makes a good paper (analyze how abstracts are written) and suggest improvements
    - Very long term could make an assistant that helps you be a better researcher (including asking the right questions)


## Possible Extensions and Improvements

### Trends

- Consider storing past trends and computing popularity rank for concepts
- Try to predict trends or trend coefficients


### Categories

- Currently everything is done with cs categories only (even for importing to database – see default arguments of XMLInterface.read()), this could be extended.
- Instead of considering more categories, could also analyze subcategories differently, e.g. show which concepts are important in computer vision and NLP


## Experiments

Our experiments are focused on making further use of the data we have.


### Status

- It works quite fine to find related concepts, simply by giving the most frequently co-occurring ones
- Clustering on concepts works a bit, but not that great
    - Note that this is also quite hard since there are various types of concepts and it is not clear what the desired result would be


### Potential Goals

- Cluster concepts based on their trend data (not NLP but could still be quite useful to group concepts together)
- Identify terms that are not matched as abbreviations but still meaningful (e.g. COVID-19)
- Something easy: Cluster papers and find most similar papers to a given one
    - This could help to summarize trends (paper clusters might correspond well to different concepts)
    - Could be interesting to propose most related papers from other subcategories to bridge gaps
- Topic modeling (similar to clustering papers this could help to summarize trends)
- Conditional vectors using subcategories as context, to find which terms have different meanings
- Identify:
    - Tasks and challenges
        - could start with some seed terms like "recognition" and "detection" and take all glossary terms ending with these words as tasks
        - similarly, terms preceding "challenge" are likely to refer to a task
        - Ultimately, we would want to decide based on the context of usage whether a given term is a task, method, dataset or else
    - Methods
    - Datasets
