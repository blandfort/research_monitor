import logging
import threading
import datetime
import re
import os
import json
import threading

from flask import Flask, request 

from content import Content
from update import Updater
from database import DB
from glossary import Glossary
from trends import Trends

import logger

log = logging.getLogger(__name__)

app = Flask(__name__)


# On start up, make sure that the database is set up correctly
with DB() as db:
    db.set_up()

Upd = Updater()


# Load mapping of arXiv category names
arxiv_categories = [line.strip().split('\t') for line in open('arxiv_categories.txt', 'r')]
arxiv_categories = {abbreviation.strip(): name.strip() for abbreviation,name in arxiv_categories}


def spell_out_category(content):
    """Utility function to spell out the category name of arXiv categories.

    Takes a Content element and returns the same elements with
    the category entry adjusted."""
    if content['category'] in arxiv_categories:
        content['category'] = arxiv_categories[content['category']]
    return content


@app.route("/health")
def health():
    return "I'm up!"

@app.route("/update_status")
def status():
    return Upd.get_status()

@app.route("/updated")
def updated():
    date = DB.newest_date

    if date is not None:
        return Content.day_formatting(date)
    else:
        return '1900-01-01'

@app.route("/trend", methods=['GET'])
def trend():
    enabled_kwargs = ['query', 'from_date', 'to_date']
    kwargs = {}

    for kwarg in enabled_kwargs:
        value = request.args.get(kwarg)

        if value is not None:
            if kwarg=='from_date' or kwarg=='to_date':
                kwargs[kwarg] = datetime.date(*map(int, value.split('-')))
            else:
                kwargs[kwarg] = value

    with DB() as db:
        dates, trend = db.get_trend(**kwargs)

    return json.dumps([[Content.day_formatting(date) for date in dates], list(map(int, trend))])

@app.route("/popular", methods=['GET'])
def popular():
    """Get the most or least trending concepts."""
    enabled_kwargs = ['reverse_order', 'max_num']
    kwargs = {}

    for kwarg in enabled_kwargs:
        value = request.args.get(kwarg)

        if value is not None:
            if kwarg=='reverse_order':
                if value.lower()=='true':
                    kwargs[kwarg] = True
                else:
                    kwargs[kwarg] = False
            else:
                kwargs[kwarg] = value

    t = Trends()
    return json.dumps(t.get_occurrences(**kwargs))
    
@app.route("/trending", methods=['GET'])
def trending():
    """Get the most or least trending concepts."""
    enabled_kwargs = ['reverse_order', 'max_num']
    kwargs = {}

    for kwarg in enabled_kwargs:
        value = request.args.get(kwarg)

        if value is not None:
            if kwarg=='reverse_order':
                if value.lower()=='true':
                    kwargs[kwarg] = True
                else:
                    kwargs[kwarg] = False
            else:
                kwargs[kwarg] = value

    t = Trends()
    return json.dumps(t.get_trendiness(**kwargs))
    

@app.route("/concept", methods=['GET'])
def concept():
    """Get the definition of a given concept.

    Returns empty string if the concept doesn't occur in the Glossary."""
    name = request.args.get('name')

    if name is not None:
        gl = Glossary()
        concept = gl.get_concept(name)
        return json.dumps(concept)
    return ''

@app.route("/concept_names", methods=['GET'])
def concept_names():
    gl = Glossary()
    names = gl.get_names()

    return '\n'.join(list(names))

@app.route("/glossary", methods=['GET'])
def glossary():
    enabled_kwargs = ['startswith']
    kwargs = {}

    for kwarg in enabled_kwargs:
        value = request.args.get(kwarg)

        if value is not None:
            kwargs[kwarg] = value

    kwargs['max_num'] = 1000  # Don't give more than 1k concepts at once

    gl = Glossary()
    contents = gl.get(**kwargs)

    return json.dumps(contents)

@app.route("/get", methods=['GET'])
def get():
    enabled_kwargs = ['query', 'date', 'keyword', 'order_by', 'reverse_order', 'max_num',
                    'category', 'from_date', 'to_date', 'offset', 'author']
    kwargs = {}

    for kwarg in enabled_kwargs:
        value = request.args.get(kwarg)

        if value is not None:
            if kwarg=='query':
                # For string query replace all non-word characters by whitespace
                kwargs[kwarg] = re.sub(r'[^0-9a-zA-Z"-]', ' ', value)
            elif kwarg=='reverse_order':
                if value.lower()=='true':
                    kwargs[kwarg] = True
                else:
                    kwargs[kwarg] = False
            else:
                kwargs[kwarg] = value

    with DB() as db:
        contents = db.get(**kwargs)

    return '\n'.join([spell_out_category(content).to_json() for content in contents])

@app.route("/reindex", methods=['POST'])
def reindex():
    """Recreate the ElasticSearch index."""
    if Upd.status!=Upd.IDLE_STATUS:
        log.warning("Updater already busy! Did not start reindexing.")
        return "Could not start this process. Is there a running update?"
    
    thread = threading.Thread(target=Upd.reindex)
    thread.start()

    return "Started to recreate the ElasticSearch index."

@app.route("/update", methods=['POST'])
def update():
    if Upd.status!=Upd.IDLE_STATUS:
        log.warning("Updater already busy! Did not start new update.")
        return "Could not start update. Update already in progress?"

    if 'type' in request.form and request.form['type'] in ['xml', 'db', 'glossary', 'trends']:
        typ = request.form['type']
    else:
        typ = 'all'

    kwargs = {}

    if 'from' in request.form:
        r = request.form['from'].split('.')
        if len(r)!=3:
            return "Invalid request!"
        kwargs['earliest_date'] = datetime.datetime(*map(int, r), hour=0, minute=0)

    if 'to' in request.form:
        r = request.form['to'].split('.')
        if len(r)!=3:
            return "Invalid request!"
        kwargs['latest_date'] = datetime.datetime(*map(int, r), hour=23, minute=59)
    kwargs['typ'] = typ

    thread = threading.Thread(target=Upd.update, kwargs=kwargs)
    thread.start()

    return "Started to update %s."%typ

