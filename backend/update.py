import logging
import configparser
import datetime
import json
import re

from content import Content
from database import DB
from crawler.interface import XMLInterface
from glossary import Glossary
from trends import Trends

config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read('config.ini')

log = logging.getLogger(__name__)

today = datetime.date.today()


class Updater:
    """Class to run updates in threads."""

    IDLE_STATUS = 'idle'
    BUSY_STATUS = 'busy'
    ABORT_STATUS = 'aborting'

    def __init__(self):
        self.reset_status()

    def get_status(self):
        message = self.status
        if self.status_info:
            message += ' (%s)'%self.status_info
        return message

    def reset_status(self):
        self.status = self.IDLE_STATUS
        self.status_info = ''  # used for providing additional information

    def is_idle(self):
        return self.status==self.IDLE_STATUS

    def reindex(self):
        try:
            self.status = self.BUSY_STATUS
            self.status_info = 'recreating index'

            with DB() as db:
                db.recreate_index()

        except Exception as e:
            raise e
        finally:
            self.reset_status()
        return True

    def update(self, typ='all', **kwargs):
        try:
            if typ=='xml':
                update_fct = update_xml
            elif typ=='db':
                update_fct = update_db
            elif typ=='glossary':
                update_fct = update_glossary
            elif typ=='trends':
                update_fct = update_trends
            elif typ=='all':
                update_fct = update_all
            else:
                raise Exception("Update type not supported!")

            log.info("Starting to update %s ..." % typ)

            self.status = self.BUSY_STATUS
            self.status_info = typ + ' – ' + str(kwargs)
            update_fct(**kwargs)
        except Exception as e:
            raise e
        finally:
            self.reset_status()
        return True


def update_all(earliest_date=None, latest_date=None):
    update_xml(earliest_date=earliest_date, latest_date=latest_date)
    update_db(earliest_date=earliest_date, latest_date=latest_date)
    update_glossary(earliest_date=earliest_date, latest_date=latest_date)
    update_trends()


def update_xml(earliest_date=None, latest_date=None):
    if latest_date is None:
        latest_date = datetime.date.today()
    if earliest_date is None:
        earliest_date = latest_date-datetime.timedelta(weeks=1)

    log.info(f"Scraping papers from {str(earliest_date)} to {str(latest_date)} ...")
    xmli = XMLInterface()
    xmli.update(earliest_month=earliest_date, latest_month=latest_date)
    log.info("Scraping finished.")

def update_db(earliest_date=None, latest_date=None):
    if latest_date is None:
        latest_date = datetime.date.today()
    if earliest_date is None:
        earliest_date = latest_date-datetime.timedelta(weeks=1)

    log.info("Connecting to the database ...")
    with DB() as db:
        log.info("Importing data into the database ...")

        xmli = XMLInterface()
        papers = xmli.read(earliest_date=earliest_date, latest_date=latest_date)

        for paper in papers:
            db.add(paper, update_newest_date=False)
        DB.newest_date = db._get_newest_date()

        log.info(f"Read {len(papers)} elements into the database.")
        log.info(f"Database now contains {db.count()} elements in total.")

def update_glossary(earliest_date=None, latest_date=None, **kwargs):
    if latest_date is None:
        #with DB() as db:
        #    contents = db.get(order_by='published', max_num=1, reverse_order=True)
        #latest_date = contents[0]['published']
        latest_date = datetime.date.today()
    if earliest_date is None:
        earliest_date = latest_date - datetime.timedelta(weeks=1)

    glossary = Glossary()

    log.info("Updating glossary ...")
    glossary.update(earliest_date=earliest_date, latest_date=latest_date, **kwargs)

def update_trends(concepts=None):
    t = Trends()

    if concepts is None:
        glossary = Glossary()
        concepts = [glossary._concept_occurrence_query(concept) for concept in glossary.get()]

        # Remove some variations to avoid nearly identical concepts as separate entries
        concepts = [re.sub(r'\s+', ' ', re.sub(r'-', ' ', concept)) for concept in concepts]

        t.update(extra_concepts=concepts)
    else:
        t.update(concepts=concepts)

